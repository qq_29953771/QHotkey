var searchData=
[
  ['json_5ftype',['json_type',['https://doc.qt.io/qt-5/struct__qjsonserializer__helpertypes_1_1json__type.html',0,'_qjsonserializer_helpertypes']]],
  ['json_5ftype_3c_20qlist_3c_20t_20_3e_20_3e',['json_type&lt; QList&lt; T &gt; &gt;',['https://doc.qt.io/qt-5/struct__qjsonserializer__helpertypes_1_1json__type_3_01_q_list_3_01_t_01_4_01_4.html',0,'_qjsonserializer_helpertypes']]],
  ['json_5ftype_3c_20qmap_3c_20qstring_2c_20t_20_3e_20_3e',['json_type&lt; QMap&lt; QString, T &gt; &gt;',['https://doc.qt.io/qt-5/struct__qjsonserializer__helpertypes_1_1json__type_3_01_q_map_3_01_q_string_00_01_t_01_4_01_4.html',0,'_qjsonserializer_helpertypes']]],
  ['json_5ftype_3c_20qpair_3c_20t1_2c_20t2_20_3e_20_3e',['json_type&lt; QPair&lt; T1, T2 &gt; &gt;',['https://doc.qt.io/qt-5/struct__qjsonserializer__helpertypes_1_1json__type_3_01_q_pair_3_01_t1_00_01_t2_01_4_01_4.html',0,'_qjsonserializer_helpertypes']]],
  ['json_5ftype_3c_20qpointer_3c_20t_20_3e_20_3e',['json_type&lt; QPointer&lt; T &gt; &gt;',['https://doc.qt.io/qt-5/struct__qjsonserializer__helpertypes_1_1json__type_3_01_q_pointer_3_01_t_01_4_01_4.html',0,'_qjsonserializer_helpertypes']]],
  ['json_5ftype_3c_20qsharedpointer_3c_20t_20_3e_20_3e',['json_type&lt; QSharedPointer&lt; T &gt; &gt;',['https://doc.qt.io/qt-5/struct__qjsonserializer__helpertypes_1_1json__type_3_01_q_shared_pointer_3_01_t_01_4_01_4.html',0,'_qjsonserializer_helpertypes']]],
  ['json_5ftype_3c_20t_20_2a_3e',['json_type&lt; T *&gt;',['https://doc.qt.io/qt-5/struct__qjsonserializer__helpertypes_1_1json__type_3_01_t_01_5_01_4.html',0,'_qjsonserializer_helpertypes']]],
  ['json_5ftype_5fraw',['json_type_raw',['https://doc.qt.io/qt-5/struct__qjsonserializer__helpertypes_1_1json__type__raw.html',0,'_qjsonserializer_helpertypes']]]
];
