var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "abcdefgijklmnopqrstuvw",
  2: "_q",
  3: "klrs",
  4: "abcdefghijklmnopqrstuvwxyz~",
  5: "abcdefghijklmnopqrstuvw",
  6: "cfhip",
  7: "cdempsv",
  8: "abcdefghijklmnopqrstuvwxyz",
  9: "o",
  10: "iqt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "properties",
  9: "related",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Properties",
  9: "Friends",
  10: "Pages"
};

