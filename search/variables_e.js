var searchData=
[
  ['offsetdatalist',['OffsetDataList',['https://doc.qt.io/qt-5/qtimezone.html#OffsetDataList-typedef',0,'QTimeZone']]],
  ['openglfeatures',['OpenGLFeatures',['https://doc.qt.io/qt-5/qopenglfunctions.html#OpenGLFeature-enum',0,'QOpenGLFunctions']]],
  ['openmode',['OpenMode',['https://doc.qt.io/qt-5/qiodevice.html#OpenModeFlag-enum',0,'QIODevice']]],
  ['operations',['Operations',['https://doc.qt.io/qt-5/qt3drender-qmemorybarrier.html#Operation-enum',0,'Qt3DRender::QMemoryBarrier']]],
  ['optimizationflags',['OptimizationFlags',['https://doc.qt.io/qt-5/qgraphicsview.html#OptimizationFlag-enum',0,'QGraphicsView']]],
  ['options',['Options',['https://doc.qt.io/qt-5/qfiledialog.html#Option-enum',0,'QFileDialog::Options()'],['https://doc.qt.io/qt-5/qfileiconprovider.html#Option-enum',0,'QFileIconProvider::Options()']]],
  ['orientations',['Orientations',['https://doc.qt.io/qt-5/qt.html#Orientation-enum',0,'Qt']]]
];
